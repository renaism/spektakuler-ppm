<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class EventController extends Controller
{
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api')]);
    }

    public function index()
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        $response = $this->client->request('GET', 'ppm/event/list');
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $events = collect($responseJSON->data)->sortByDesc('updated_at');
            return view('events.index', ['events' => $events]);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function create()
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'staff') {
            return redirect('/events')->with('error', 'Hanya staff yang bisa buat event');
        }    
        return view('events.create');
    }

    public function store(Request $request)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'staff') {
            return redirect('/events')->with('error', 'Hanya staff yang bisa buat event');
        }       
        $response = $this->client->request('POST', 'ppm/event/add', [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $request->input()
        ]);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            return redirect('/events')->with('success', 'Event berhasil ditambahkan');
        }
        else {
            return redirect('/events')->with('error', 'Event gagal ditambahkan');
        }
    }

    public function show($id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        $response = $this->client->request('GET', 'ppm/event/'.$id);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $event = $responseJSON->data;
            if (Session::get('user') == 'dosen') {
                $event->papers = collect($event->papers)->filter(function($paper) {
                    return $paper->nip_dosen == Session::get('id');
                });
            }
            else if(Session::get('user') == 'mahasiswa') {
                $r = $this->client->request('GET', 'ppm/paper/subwriter/'.Session::get('id'));
                $rJSON = json_decode($r->getBody());
                $subwriters = collect($rJSON->data);
                $papers_mhs = $subwriters->map(function($sw) {
                    return $sw->id_paper;
                });
                $event->papers = collect($event->papers)->filter(function($paper) use ($papers_mhs) {
                    return $papers_mhs->contains($paper->id_paper);
                });
            }
            return view('events.show', ['event' => $event]);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function edit($id)
    {
        /*
        $response = $this->client->request('GET', 'ppm/event/'.$id);
        if ($response->getStatusCode() == 200) {
            $responseJSON = json_decode($response->getBody());
            $event = $responseJSON->data;
            return view('events.edit');
        }
        */
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
