<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api')]);    
    }

    public function index()
    {
        //
    }

    public function create($id_paper)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'evaluator') {
            return redirect('/papers/'.$id_paper)->with('error', 'Hanya evaluator yang bisa buat review');
        }   
        
        return view('reviews.create', ['id_paper' => $id_paper]);
    }

    public function store(Request $request)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        $input = $request->input();
        if (Session::get('user') != 'evaluator') {
            return redirect('/papers/'.$input['id_paper'])->with('error', 'Hanya evaluator yang bisa buat review');
        }
        
        $id_evaluator = Session::get('id');
        
        $response = $this->client->request('POST', 'ppm/paper/'.$input['id_paper'].'/review/add/'.$id_evaluator, [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $input
        ]);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            return redirect('/papers/'.$input['id_paper'])->with('success', 'Review berhasil ditambahkan');
        }
        else {
            return redirect('/papers/'.$input['id_paper'])->with('error', 'Review gagal ditambahkan');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
