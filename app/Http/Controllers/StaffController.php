<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api')]);    
    }

    public function index()
    {

    }

    public function create()
    {
        return view('staffs.create');
    }

    public function store(Request $request)
    {
        $response = $this->client->request('POST', 'ppm/staff/add', [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $request->input()
        ]);

        return $response->getBody();
    }

    public function show($username)
    {   
        $response = $this->client->request('GET', $username);

        return $response->getBody();
    }

    public function edit($username)
    {
        //
    }

    public function update(Request $request, $username)
    {
        $response = $this->client->request('PUT', 'ppm/staff/'.$username.'/edit', [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $request->input()
        ]);

        return $response->getBody();
    }

    public function remove($username)
    {
        $response = $this->client->request('DELETE', 'ppm/staff/'.$username.'/remove');

        return $response->getBody();
    }
}
