<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api')]);    
    }

    public function index()
    {
        if (Session::has('user')) {
            return redirect('/papers');
        }
        return view('landing');
    }

    public function loginPage($user)
    {
        if (in_array($user, ['staff', 'evaluator', 'dosen', 'mahasiswa'])) {
            return view('auth.login', ['user' => $user]);
        }
        else {
            abort(404);
        }
    }

    public function login(Request $request, $user)
    {
        $input = $request->input();

        try {
            if ($user == 'staff') {
                $response = $this->client->request('GET', 'ppm/staff/'.$input['username']);
                $responseJSON = json_decode($response->getBody());
                $staff = $responseJSON->data;
                $id = $staff->id_staff;
                $hash = $staff->password;
            }
            else if ($user == 'evaluator') {
                $response = $this->client->request('GET', 'ppm/evaluator/'.$input['username']);
                $responseJSON = json_decode($response->getBody());
                $evaluator = $responseJSON->data;
                $id = $evaluator->id_evaluator;
                $hash = $evaluator->password;
            }
            else if ($user == 'dosen') {
                // Placeholder, password = username
                $hash = password_hash($input['username'], PASSWORD_DEFAULT);
                $id = $input['username'];
            }
            else if ($user == 'mahasiswa') {
                // Placeholder, password = username
                $hash = password_hash($input['username'], PASSWORD_DEFAULT);
                $id = $input['username'];
            }
        }
        catch (\Exception  $e) {
            return redirect('/login/'.$user)->with('error', 'Can\'t find username');
        }
        
        // Verify password
        if (password_verify($input['password'], $hash)) {
            Session::put('user', $user);
            Session::put('username', $input['username']);
            Session::put('id', $id);
            return redirect('/');
        }
        else {
            return redirect('/login/'.$user)->with('error', 'Wrong password');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }
}
