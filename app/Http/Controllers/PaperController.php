<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

class PaperController extends Controller
{
    public function __construct()
    {
        $this->client = new Client(['base_uri' => config('app.api')]);    
    }

    public function index()
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        
        $response = $this->client->request('GET', 'ppm/paper/list');
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $papers = collect($responseJSON->data);
            # Staff: pending first
            if (Session::get('user') == 'staff') {
                $papers = $papers->filter(function($paper) {
                    return $paper->status == 'pending';
                });
                $caption = 'Daftar Paper Pending';
            }
            # Evalutor: verified->revised->for-revision->approved
            else if (Session::get('user') == 'evaluator') {
                $papers = $papers->filter(function($paper) {
                    return $paper->status == 'verified' || $paper->status == 'revised' || $paper->status == 'for-revision';
                });
                $papers = $papers->sortBy(function ($paper) {
                    if ($paper->status == 'verified') return 0;
                    else if ($paper->status == 'revised') return 1;
                    else return 2;
                });
                $caption = 'Daftar Paper Untuk Direview';
            }
            # Dosen: for-revision->pending->revised->verified
            else if (Session::get('user') == 'dosen' || Session::get('user') == 'mahasiswa') {
                if(Session::get('user') == 'dosen') {
                    $id_dosen = Session::get('id');
                    $papers = $papers->filter(function($paper) use ($id_dosen) {
                        return $paper->nip_dosen == $id_dosen;
                    });
                }
                else {
                    $r = $this->client->request('GET', 'ppm/paper/subwriter/'.Session::get('id'));
                    $rJSON = json_decode($r->getBody());
                    $subwriters = collect($rJSON->data);
                    $papers_mhs = $subwriters->map(function($sw) {
                        return $sw->id_paper;
                    });
                    $papers = $papers->filter(function($paper) use ($papers_mhs) {
                        return $papers_mhs->contains($paper->id_paper);
                    });
                }
                $papers = $papers->sortBy(function ($paper) {
                    if ($paper->status == 'for-revision') return 0;
                    else if ($paper->status == 'pending') return 1;
                    else if ($paper->status == 'revised') return 2;
                    else if ($paper->status == 'verified') return 3;
                    else return 4;
                });
                $caption = 'Daftar Paper Saya';
            }
            
            return view('papers.index', ['papers' => $papers, 'caption' => $caption]);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function create($id_event)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'dosen') {
            return redirect('/events/'.$id_event)->with('error', 'Hanya dosen yang bisa submit paper');
        }

        return view('papers.create', ['id_event' => $id_event]);
    }

    public function store(Request $request)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }

        $input = $request->input();

        if (Session::get('user') != 'dosen') {
            return redirect('/events/'.$input['id_event'])->with('error', 'Hanya dosen yang bisa submit paper');
        }
        
        $input['nip_dosen'] = Session::get('id');
        $input['date'] = date('Y-m-d');

        // Handlie file upload
        if($request->hasFile('document')) {
            // Get file name with the extension
            $fNameExt = $request->file('document')->getClientOriginalName();
            // Get just the file name
            $fName = pathinfo($fNameExt, PATHINFO_FILENAME);
            // Get just the extension
            $fExt = $request->file('document')->getClientOriginalExtension();
            // File name to store
            $fNameStore = 'paper_'.$fName.'_'.time().'.'.$fExt;
            // Put the image to storage
            Storage::putFileAs('events/'.$input['id_event'].'/papers', $request->file('document'), $fNameStore);
            
            $input['file_path'] = $fNameStore;
        }
        else {
            $input['file_path'] = null;
        }
        $response = $this->client->request('POST', 'ppm/paper/add', [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $input
        ]);

        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            return redirect('/events/'.$input['id_event'])->with('success', 'Paper berhasil ditambahkan');
        }
        else {
            return redirect('/events/'.$input['id_event'])->with('error', 'Paper gagal ditambahkan');
        }
    }

    public function show($id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        
        $response = $this->client->request('GET', 'ppm/paper/'.$id);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $paper = $responseJSON->data;
            if (Session::get('user') == 'dosen' && $paper->nip_dosen != Session::get('id')) {
                return redirect('/events/'.$paper->id_event)->with('error', 'Anda tidak berhak melihat paper tersebut');
            }
            else if(Session::get('user') == 'mahasiswa') {
                $r = $this->client->request('GET', 'ppm/paper/subwriter/'.Session::get('id'));
                $rJSON = json_decode($r->getBody());
                $subwriters = collect($rJSON->data);
                $papers_mhs = $subwriters->map(function($sw) {
                    return $sw->id_paper;
                });
                if (!$papers_mhs->contains($paper->id_paper)) {
                    return redirect('/events/'.$paper->id_event)->with('error', 'Anda tidak berhak melihat paper tersebut');
                }
            }
            return view('papers.show', ['paper' => $paper]);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function download($id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        
        $response = $this->client->request('GET', 'ppm/paper/'.$id);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $paper = $responseJSON->data;
            return Storage::download('/events/'.$paper->id_event.'/papers/'.$paper->file_path, $paper->title);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function edit($id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'dosen') {
            return redirect('/papers/'.$id)->with('error', 'Hanya dosen yang bisa update paper');
        }
        
        $response = $this->client->request('GET', 'ppm/paper/'.$id);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $paper = $responseJSON->data;
            return view('papers.create', ['id_event' => $paper->id_event, 'paper' => $paper]);
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function update(Request $request, $id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'dosen') {
            return redirect('/papers/'.$id)->with('error', 'Hanya dosen yang bisa update paper');
        }
        $response = $this->client->request('GET', 'ppm/paper/'.$id);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            $paper = $responseJSON->data;
            if($request->hasFile('document')) {
                Storage::putFileAs('events/'.$paper->id_event.'/papers', $request->file('document'), $paper->file_path);
                return redirect('/papers/'.$id)->with('success', 'Paper berhasil di-update');
            }
        }
        return redirect('/papers/'.$id)->with('error', 'Paper gagal di-update');
    }

    public function verify($id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'staff') {
            return redirect('/papers/'.$id)->with('error', 'Hanya staff yang bisa memverifikasi paper');
        }   
        $id_staff = Session::get('id');
        $response = $this->client->request('PUT', 'ppm/paper/'.$id.'/verify/'.$id_staff);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            return redirect('/papers/'.$id)->with('success', 'Paper terverifikasi');
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function updateStatus(Request $request, $id)
    {
        if (!Session::has('user')) {
            return redirect('/');
        }
        if (Session::get('user') != 'staff' && Session::get('user') != 'evaluator' && Session::get('user') != 'dosen') {
            return redirect('/papers/'.$id)->with('error', 'Hanya staff/evaluator yang bisa mengubah status paper');
        }   
        
        $response = $this->client->request('PUT', 'ppm/paper/'.$id.'/changeStatus', [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'form_params' => $request->input()
        ]);
        $responseJSON = json_decode($response->getBody());
        if ($responseJSON->success == 'true') {
            return redirect('/papers/'.$id)->with('success', 'Status paper berhasil diubah');;
        }
        else {
            abort($response->getStatusCode());
        }
    }

    public function destroy($id)
    {
        //
    }
}
