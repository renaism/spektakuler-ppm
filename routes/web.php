<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');
Route::get('/login/{user}', 'UserController@loginPage');
Route::post('/login/{user}', 'UserController@login');
Route::get('/logout', 'UserController@logout');
Route::resource('staffs', 'StaffController');
Route::resource('events', 'EventController');
Route::resource('papers', 'PaperController');
Route::get('/papers/create/{id_event}', 'PaperController@create');
Route::put('/papers/{id}/verify', 'PaperController@verify');
Route::put('/papers/{id}/updateStatus', 'PaperController@updateStatus');
Route::get('/papers/{id}/download', 'PaperController@download');
Route::resource('reviews', 'ReviewController');
Route::get('/reviews/create/{id_paper}', 'ReviewController@create');
Route::get('/test', function() {
    return view('papers.index');
});