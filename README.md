# Spektakuler | Modul PPM

## API
https://github.com/wisn/spektakuler/tree/modul-ppm

## Setup Database Lokal
Kalau API-nya di-run secara lokal, jangan lupa import file `spektakuler-ppm.sql` ke database.

## Setup
1. Download API di atas dan modul ini (bisa `git clone` atau download zip)
2. Setup API  di folder `/spektakuler` berdasarkan petunjuk di https://github.com/wisn/spektakuler/tree/modul-ppm#panduan-pemasangan
3. Di folder modul (`/spektakuler-ppm`), jalankan satu-satu perintah berikut pada terminal:
```
composer install
cp .env.example .env
php artisan key:generate
```
4. Jalankan server modul:
```
php artisan server
```
5. Pada folder API (`/spektakuler`), jalankan server API:
```
php -S localhost:8080 -t public
```
6. Akses modul pada browser dengan alamat http://localhost:8000

## Akun
Semua akun yang udah ada di database passwordnya "12345" khusus untuk staff dan evaluator.

Untuk mahasiswa dan dosen, usename bebas dan passwordnya sama dengan username. (Berhubung modul hr dan sm belum ada endpoint yang bisa dapetin hanya satu dosen/mahasiswa).

## Catatan
Kalau mau akses API yang di web ( `!` Mungkin belum di-update) ubah API_URL di file `.env` jadi:
```
API_URL='https://spektakuler-staging.herokuapp.com/api/v1/'
```
