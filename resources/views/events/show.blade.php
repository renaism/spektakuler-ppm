@extends('layouts.app')
@section('title', 'Events - '.$event->name)
@section('content')
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="/events">Event</a>
    </li>
    <li class="breadcrumb-item active">{{ $event->name }}</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        {{ $event->name }}
    </div>
    <div class="card-body">
    <strong>Dari:</strong> {{ date("d F Y", strtotime($event->start_date)) }} <br>
    <strong>Hingga:</strong> {{ date("d F Y", strtotime($event->end_date)) }} <br>
    <br>
    <strong>Deskripsi:</strong><br>
    {{ $event->description }}
    </div>
</div>
<div class="card mb-3">
    <div class="card-header">List Paper</div> 
    <div class="card-body">
    @if(Session::get('user') == 'dosen')
        <a href="/papers/create/{{ $event->id_event }}" class="btn btn-primary mb-3">Submit Paper</a>
    @endif
        <table style="width:100%" class="tableview">
            <tr>
            <th><b>Paper</b></th> 
            <th><b>NIP Dosen</b></th>
            <th><b>Tanggal</b></th>
            <th><b>Status</b></th>
            <th></th>
            </tr>
            @foreach ($event->papers as $paper)
                <tr>
                    <td>{{ $paper->title }}</td> 
                    <td>{{ $paper->nip_dosen }}</td>
                    <td>{{ date("d M Y", strtotime($paper->date)) }}</td>
                    <td>{{ ucfirst($paper->status) }}</td>
                    <td><a href="/papers/{{ $paper->id_paper }}" class="btn btn-primary">View</a></td>
                </tr>
            @endforeach
        </table>                  
    </div>
</div>
@endsection