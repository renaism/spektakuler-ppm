@extends('layouts.app')
@section('title', 'New Event')
@section('content')
<div class="card card-register mx-auto mt-5">
    <div class="card-header">Input New Event</div>
    <div class="card-body">
    <form method="POST" action="{{ action('EventController@store') }}">
        @csrf
        <div class="form-group">
            <div class="form-label-group">
                <input type="text" id="name" name="name" class="form-control" placeholder="Nama Event" required="required" autofocus="autofocus">
                <label for="name">Nama Event</label>
            </div>
        </div>
        <div class="form-group">
            <label>Deskripsi</label>
            <div class="form-label-group">
                <textarea class="form-control" id="description" name="description" rows="3" required="required"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col-12 col-md-6 mb-3 mb-md-0">
                    <div class="form-label-group">
                        <input type="date" id="start_date" name="start_date" class="form-control" required="required">
                        <label for="start_date">Tanggal Mulai</label>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-label-group">
                        <input type="date" id="end_date" name="end_date" class="form-control" required="required">
                        <label for="end_date">Tanggal Berakhir</label>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Input</button>
    </form>
    <div class="text-center">
        <a class="d-block small mt-3" href="/events">Kembali</a>
    </div>
    </div>
</div>
@endsection