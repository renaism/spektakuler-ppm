@extends('layouts.app')
@section('title', 'Events')
@section('content')
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a class="breadcrumb-item active">Event</a>
    </li>   
</ol>
@if(Session::get('user') == 'staff')
    <a href="/events/create" class="btn btn-primary mb-3">New Event</a>
@endif
@forelse ($events as $event)
    <a data-toggle="nav-link" href="/events/{{ $event->id_event }}">
        <div class="card mb-3">
            <div class="card-header">
                {{ $event->name }}
            </div>
            <div class="card-footer small text-muted">
                {{ date("d F Y", strtotime($event->start_date)) }} - {{ date("d F Y", strtotime($event->end_date)) }}
            </div>
        </div>
    </a>
@empty
    <h2>No event to display :(</h2>
@endforelse
@endsection