<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Landing</title>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <center><div class="card-header">Penelitian Dan Pengabdian Masyarakat</div>
      <div class="card-body">
          <img src="{{ asset('tel-u.png') }}" height="120" width="auto">	
          <br>
          <h4>Login</h4>	
          <a class="btn btn-success btn-block" href="/login/staff" >Staff</a>
          <a class="btn btn-info btn-block" href="/login/evaluator" >Evaluator</a>
          <a class="btn btn-warning btn-block" href="/login/dosen" >Dosen</a>
          <a class="btn btn-danger btn-block" href="/login/mahasiswa" >Mahasiswa</a>
       </div>
       </div>
      </center>
      </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin.min.js') }}"></script>

</body>

</html>
