@extends('layouts.app')
@section('title', 'Submit Paper')
@section('content')
<div class="card card-register mx-auto mt-5">
    @empty($paper)
        <div class="card-header">Submit New Paper</div>
    @else
        <div class="card-header">Edit Paper</div>
    @endempty
    
    <div class="card-body">
    @empty($paper)
        <form method="POST" action="{{ action('PaperController@store') }}" enctype="multipart/form-data">
    @else
        <form method="POST" action="/papers/{{ $paper->id_paper }}" enctype="multipart/form-data">
        @method('PUT')
    @endempty
        @csrf
        <input name="id_event" type="hidden" value="{{ $id_event }}">
        <div class="form-group">
            <div class="form-label-group">
                <input type="text" id="title" name="title" class="form-control" placeholder="Judul" required="required" @isset($paper) value="{{ $paper->title }}" readonly @else autofocus="autofocus" @endisset>
                <label for="title">Judul</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-label-group">
                <input type="number" id="fund" name="fund" class="form-control" placeholder="Dana" required="required" @isset($paper) value="{{ $paper->fund }}" readonly @endisset>
                <label for="fund">Dana</label>
            </div>
        </div>
        <div class="form-group">
                <div class="form-label-group">
                    <input type="text" id="nip_dosen" name="nip_dosen" class="form-control" readonly required="required" value="@empty($paper){{ Session::get('id') }}@else{{ $paper->nip_dosen }}@endisset">
                    <label for="nip_dosen">NIP Penulis 1</label>
                </div>
            </div>
        <div class="form-group">
            <div class="form-label-group">
                <input type="text" id="nim_mahasiswa1" name="nim_mahasiswa[]" class="form-control" placeholder="NIM Penulis 2" @if(isset($paper) && isset($paper->subwriters[0])) value="{{ $paper->subwriters[0]->nim_mahasiswa }}" readonly @endif>
                <label for="nim_mahasiswa1">NIM Penulis 2</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-label-group">
                <input type="text" id="nim_mahasiswa2" name="nim_mahasiswa[]" class="form-control" placeholder="NIM Penulis 3" @if(isset($paper) && isset($paper->subwriters[1])) value="{{ $paper->subwriters[1]->nim_mahasiswa }}" readonly @endif>
                <label for="nim_mahasiswa2">NIM Penulis 3</label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-label-group">
                <input type="text" id="nim_mahasiswa3" name="nim_mahasiswa[]" class="form-control" placeholder="NIM Penulis 4" @if(isset($paper) && isset($paper->subwriters[2])) value="{{ $paper->subwriters[2]->nim_mahasiswa }}" readonly @endif>
                <label for="nim_mahasiswa3">NIM Penulis 4</label>
            </div>
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="document" id="documentFile" accept=".pdf" required>
                <label class="custom-file-label" for="customFile">
                    @empty($paper)
                        Unggah Berkas Dokumen
                    @else
                        Unggah Ulang Berkas Dokumen
                    @endempty
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">@empty($paper) Input @else Edit @endempty</button>
    </form>
    <div class="text-center">
        @empty($paper)
            <a class="d-block small mt-3" href="/events/{{ $id_event }}">Kembali</a>
        @else
            <a class="d-block small mt-3" href="/papers/{{ $paper->id_paper }}">Kembali</a>
        @endempty
    </div>
    </div>
</div>
@endsection