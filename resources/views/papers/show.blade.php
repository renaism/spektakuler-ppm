@extends('layouts.app')
@section('title', 'Papers - '.$paper->title)
@section('content')
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
    <a href="/events">Event</a>
    </li>
    <li class="breadcrumb-item active">
    <a href="/events/{{ $paper->event->id_event }}">{{ $paper->event->name }}</a>
    </li>
    <li class="breadcrumb-item active">{{ $paper->title }}</li>
</ol>

<div class="card mb-3">
    <div class="card-header d-flex justify-content-between">{{ $paper->title }}</div>
    <div class="card-body">
        <strong>NIP Penulis 1:</strong> {{ $paper->nip_dosen }}<br>
        @if ($paper->subwriters)
            @foreach ($paper->subwriters as $subwriter)
                <strong>NIM Penulis {{ $loop->iteration+1 }}:</strong> {{ $subwriter->nim_mahasiswa }}<br>
            @endforeach
        @endif
        <strong>Dana:</strong> Rp{{ number_format($paper->fund) }},- <br>
        <strong>Tanggal:</strong> {{ date("d F Y", strtotime($paper->date)) }} <br>
        @if ($paper->status == 'pending')
            <strong>Status:</strong> {{ ucfirst($paper->status) }} <br>
            @if (Session::get('user') == 'staff')
                <form action="/papers/{{ $paper->id_paper }}/verify" method="post" class="mt-3">
                    @method('PUT')
                    @csrf
                    <button type="submit" class="btn btn-success mb-3">Verifikasi</button>
                </form>
            @endif
        @elseif($paper->status == 'for-revision')
            <strong>Status:</strong> {{ ucfirst($paper->status) }} <br>
            @if (Session::get('user') == 'dosen')
                <form action="/papers/{{ $paper->id_paper }}/updateStatus" method="post" class="mt-3">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="status" value="revised">
                    <button type="submit" class="btn btn-success mb-3">Revisi</button>
                </form>
            @endif
        @else
            @if (Session::get('user') == 'staff' || Session::get('user') == 'evaluator')
                <form action="/papers/{{ $paper->id_paper }}/updateStatus" method="post" class="form-inline mt-3">
                    @method('PUT')
                    @csrf
                    <label class="mb-2 mr-sm-2"><strong>Status:</strong></label>
                    <select name="status" class="custom-select mb-2 mr-sm-2" placeholder="jenis event" required="required">
                        <option value="pending" @if($paper->status == 'pending') selected @endif>Pending</option>
                        <option value="verified" @if($paper->status == 'verified') selected @endif>Verified</option>
                        <option value="for-revision" @if($paper->status == 'for-revision') selected @endif>For Revision</option>
                        <option value="revised" @if($paper->status == 'revised') selected @endif>Revised</option>
                        <option value="approved" @if($paper->status == 'approved') selected @endif>Approved</option>
                    </select>
                    <button type="submit" class="btn btn-success mb-2 mr-sm-2">Ubah Status</button>
                </form>
            @else
                <strong>Status:</strong> {{ ucfirst($paper->status) }} <br>
            @endif
        @endif
        @if ($paper->id_staff && $paper->status != 'pending')
            <strong>Diverfikasi oleh:</strong> {{ $paper->staff->name }} <br>
        @endif
        <strong>Daftar Review:</strong> <br>
        @forelse ($paper->reviews as $review)
            <u>{{ $review->evaluator->name }}</u> :
            <p>
                {{ $review->review }} <br>
                <small>{{ date("d F Y", strtotime($review->updated_at)) }}</small>
            </p>
        @empty
        <p>
            Belum ada review :(
        </p>    
        @endforelse
        @if (Session::get('user') == 'evaluator')
            <a href="/reviews/create/{{ $paper->id_paper }}" class="btn btn-primary mb-3">New Review</a>
        @endif
    </div>
    <div class="card-footer d-flex">
        @if ($paper->file_path)
            <a href="/papers/{{ $paper->id_paper }}/download" class="btn btn-success">Download File</a>
        @endif
        @if (Session::get('user') == 'dosen')
            <a href="/papers/{{ $paper->id_paper }}/edit" class="btn btn-primary ml-auto">Edit</a>
        @endif
    </div>
</div>
@endsection