@extends('layouts.app')
@section('title', 'Papers')
@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">{{ ucfirst(Session::get('user')) }}</li>
        </ol>
        <h3>{{ $caption }}</h3><hr>
        <!-- Page Content -->
        <table style="width:100%" class="tableview">
            <tr>
                <th><b>No<b></th>
                <th><b>Paper</b></th> 
                <th><b>Dosen</b></th>
                <th></th>
                <th><b>Status</b></th>
            </tr>
            @foreach ($papers as $paper)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $paper->title }}</td> 
                    <td>{{ $paper->nip_dosen }}</td>
                    <td><a type="button" href="/papers/{{ $paper->id_paper }}" class="btn btn-sm btn-primary">View</a></td>
                    <td>{{ ucfirst($paper->status) }}</td>
                </tr>
            @endforeach
        </table>
@endsection