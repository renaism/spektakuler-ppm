@extends('layouts.app')
@section('title', 'Staff - Sign Up')
@section('content')
<div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Staff Sign Up</div>
      <div class="card-body">
        <form method="POST" action="{{ action('StaffController@store') }}">
          @csrf
          <div class="form-group">
                <div class="form-label-group">
                  <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                  <label for="username">Username</label>
                </div>
          </div>
          <div class="form-group">
                <div class="form-label-group">
                  <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required="required" autofocus="autofocus">
                  <label for="email">E-mail</label>
                </div>
          </div>
          <div class="form-group">
                <div class="form-label-group">
                  <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required" autofocus="autofocus">
                  <label for="password">Password</label>
                </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" id="name" name="name" class="form-control" placeholder="Nama" required="required" autofocus="autofocus">
              <label for="name">Nama</label>
            </div>
      </div>
          <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="/">Kembali</a>
        </div>
      </div>
    </div>
</div>
@endsection