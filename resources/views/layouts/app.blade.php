<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name', 'App') }} | @yield('title')</title>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">{{ config('app.name', 'App') }}</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto">
      @if(Session::has('user'))
        <li class="nav-item">
          <span class="navbar-text">Logged in as {{ Session::get('username') }} ({{ ucfirst(Session::get('user')) }}) |</span>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/logout">Logout</a>
        </li>
      @else
        <li class="nav-item"><span class="navbar-text"><strong>Login |</strong></span></li>
        <li class="nav-item">
          <a class="nav-link active" href="/login/staff">Staff</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/login/evaluator">Evaluator</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/login/dosen">Dosen</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/login/mahasiswa">Mahasiswa</a>
        </li>
      @endif
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item active">
          <a class="nav-link" href="/events">
            <i class="fas fa-fw fa-calendar"></i>
            <span>Events</span>
          </a>
      </li>
    </ul>

    <div id="content-wrapper">

        <div class="container-fluid">
            @include('inc.messages')
            @yield('content')
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Subarashii 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin.min.js') }}"></script>
  <script>
      $(document).ready(function() {
          $("input:file").change(function() {
              // Display file name
              let fileName = $(this).val().split('\\').pop();;
              $(this).next('.custom-file-label').addClass('selected').html(fileName);
          });
      });
  </script>
</body>

</html>
