@extends('layouts.app')
@section('title', 'New Review')
@section('content')
<div class="card card-register mx-auto mt-5">
    <div class="card-header">Review Paper</div>
    <div class="card-body">
    <form method="POST" action="{{ action('ReviewController@store') }}">
        @csrf
        <input name="id_paper" type="hidden" value="{{ $id_paper }}">
        <div class="form-group">
                <label>Review</label>
                <div class="form-label-group">
                    <textarea class="form-control" id="review" name="review" rows="3" required="required" autofocus="autofocus"></textarea>
                </div>
            </div>
        <button type="submit" class="btn btn-primary btn-block">Input</button>
    </form>
    <div class="text-center">
        <a class="d-block small mt-3" href="/papers/{{ $id_paper }}">Kembali</a>
    </div>
    </div>
</div>
@endsection