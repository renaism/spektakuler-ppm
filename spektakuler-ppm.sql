-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2019 at 10:42 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spektakuler`
--

-- --------------------------------------------------------

--
-- Table structure for table `ppm_evaluators`
--

CREATE TABLE `ppm_evaluators` (
  `id_evaluator` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppm_evaluators`
--

INSERT INTO `ppm_evaluators` (`id_evaluator`, `username`, `email`, `password`, `name`, `created_at`, `updated_at`) VALUES
(3, 'asukarizki', 'rqrqrq@rocketmail.com', '$2y$10$k/ek6oeTt7FrV1Inf7.dk.4GWiOfRH.YHZqMmMhbyIwcwU9tjqA7S', 'Muhammad Alfarizky Sunetyo', '2019-05-15 09:28:35', '2019-05-15 09:28:35'),
(4, 'ansuci', 'bigsister@rocketmail.com', '$2y$10$DnkCuzG4oVPzH.ZVVXDa8e/LdjxDbbSIi8JLkcmalObkOjQ06zwz6', 'Annisa Suciati Salsabila', '2019-05-15 09:28:35', '2019-05-15 09:28:35'),
(5, 'cokro', 'cokro@rocketmail.com', '$2y$10$wMm35rwIZPI5yZD9JJSeJej6g1tLTaeY17VpNUQuHs8vLpQLmYR2G', 'Tjokroaminoto', '2019-05-15 09:28:35', '2019-05-15 09:28:35');

-- --------------------------------------------------------

--
-- Table structure for table `ppm_events`
--

CREATE TABLE `ppm_events` (
  `id_event` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppm_events`
--

INSERT INTO `ppm_events` (`id_event`, `name`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'PKM 2019', 'Pekan Kreativitas Mahasiswa (PKM) tahun 2019', '2019-01-01', '2019-12-31', '2019-05-03 04:24:35', '2019-05-03 04:24:35'),
(2, 'PKM 2020', 'Pekan Kreativitas Mahasiswa (PKM) tahun 2020', '2020-01-01', '2020-12-31', '2019-05-03 04:24:35', '2019-05-03 04:24:35'),
(4, 'Gemastik 2019', 'Gerakan Mahasiswa Antik (Gemastik) tahun 2019 yang diselenggarakan di Telkom University', '2019-06-01', '2019-08-15', '2019-05-15 09:31:05', '2019-05-15 09:31:05');

-- --------------------------------------------------------

--
-- Table structure for table `ppm_papers`
--

CREATE TABLE `ppm_papers` (
  `id_paper` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `fund` bigint(20) NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` set('pending','verified','approved','for-revision','revised') COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_event` int(11) NOT NULL,
  `nip_dosen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ppm_reviews`
--

CREATE TABLE `ppm_reviews` (
  `id_evaluator` int(11) NOT NULL,
  `id_paper` int(11) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ppm_staffs`
--

CREATE TABLE `ppm_staffs` (
  `id_staff` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ppm_staffs`
--

INSERT INTO `ppm_staffs` (`id_staff`, `username`, `email`, `password`, `name`, `created_at`, `updated_at`) VALUES
(4, 'rizkiar00', 'rizkiganteng@rocketmail.com', '$2y$10$uGMyivT8P2JnBijd3Q7zw.eHV55m4Spnaot11EmiFmft9pvLbmhqS', 'Rizki Achmad Riyanto', '2019-05-15 09:28:26', '2019-05-15 09:28:26'),
(5, 'zafitract', 'zafitract@rocketmail.com', '$2y$10$xCC29Qcudg/uwFLdmGcdGeV0oyWNS0aMirFYNdj91rhuXhCFWh/AG', 'Rezza Nafi', '2019-05-15 09:28:26', '2019-05-15 09:28:26'),
(6, 'fargear24', 'fargear24@rocketmail.com', '$2y$10$/CEP5vvUj8domxFUzVBbf.hSJAKLelnbXmvHJrazyuXfvYXBbaaLm', 'Farras Hafis', '2019-05-15 09:28:26', '2019-05-15 09:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `ppm_subwriters`
--

CREATE TABLE `ppm_subwriters` (
  `nim_mahasiswa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_paper` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ppm_evaluators`
--
ALTER TABLE `ppm_evaluators`
  ADD PRIMARY KEY (`id_evaluator`),
  ADD UNIQUE KEY `ppm_evaluators_username_unique` (`username`),
  ADD UNIQUE KEY `ppm_evaluators_email_unique` (`email`);

--
-- Indexes for table `ppm_events`
--
ALTER TABLE `ppm_events`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `ppm_papers`
--
ALTER TABLE `ppm_papers`
  ADD PRIMARY KEY (`id_paper`);

--
-- Indexes for table `ppm_staffs`
--
ALTER TABLE `ppm_staffs`
  ADD PRIMARY KEY (`id_staff`),
  ADD UNIQUE KEY `ppm_staffs_username_unique` (`username`),
  ADD UNIQUE KEY `ppm_staffs_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ppm_evaluators`
--
ALTER TABLE `ppm_evaluators`
  MODIFY `id_evaluator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ppm_events`
--
ALTER TABLE `ppm_events`
  MODIFY `id_event` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ppm_papers`
--
ALTER TABLE `ppm_papers`
  MODIFY `id_paper` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ppm_staffs`
--
ALTER TABLE `ppm_staffs`
  MODIFY `id_staff` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
